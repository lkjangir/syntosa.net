# README #

Syntosa.net is part of the open source product that provides a distributed temporal repository of property graphs.

### Want to contribute to Syntosa? ###

If you're interested in being a contributor and want to get involved in developing Syntosa, you must complete a contributors agreement.

### Contributor License Agreements ###
We'd love to accept your patches! Before we can take them, we have to jump a couple of legal hurdles.

Please fill out either the individual or corporate Contributor License Agreement (CLA). 

* If you are an individual writing original source code and you're sure you own the intellectual property, then you'll need to sign an individual CLA.
* If you work for a company that wants to allow you to contribute your work, then you'll need to sign a corporate CLA.

Once you are CLA'ed, we'll be able to accept your pull requests.
NOTE: Only original source code from you and other people that have signed the CLA can be accepted into the repository.

### Are you ready to add to the discussion?###

We have presence on:

* [Twitter](https://twitter.com/syntosa)
* [Medium](https://syntosahq.com)